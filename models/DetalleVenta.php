<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%detalle_venta}}".
 *
 * @property int $iddetalle_venta
 * @property int $idproducto
 * @property int $idventa
 * @property string $descripcion
 * @property int $valor
 * @property int $descuento
 * @property int $iva
 * @property int $total
 *
 * @property Producto $producto
 * @property Venta $venta
 */
class DetalleVenta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%detalle_venta}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idproducto', 'idventa', 'descripcion', 'valor', 'descuento', 'iva', 'total'], 'required'],
            [['idproducto', 'idventa', 'valor', 'descuento', 'iva', 'total'], 'integer'],
            [['descripcion'], 'string', 'max' => 200],
            [['idproducto'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['idproducto' => 'idproducto']],
            [['idventa'], 'exist', 'skipOnError' => true, 'targetClass' => Venta::className(), 'targetAttribute' => ['idventa' => 'idventa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddetalle_venta' => Yii::t('app', 'Iddetalle Venta'),
            'idproducto' => Yii::t('app', 'Idproducto'),
            'idventa' => Yii::t('app', 'Idventa'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'valor' => Yii::t('app', 'Valor'),
            'descuento' => Yii::t('app', 'Descuento'),
            'iva' => Yii::t('app', 'Iva'),
            'total' => Yii::t('app', 'Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['idproducto' => 'idproducto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVenta()
    {
        return $this->hasOne(Venta::className(), ['idventa' => 'idventa']);
    }
}
