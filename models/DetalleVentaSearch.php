<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DetalleVenta;

/**
 * DetalleVentaSearch represents the model behind the search form of `app\models\DetalleVenta`.
 */
class DetalleVentaSearch extends DetalleVenta
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddetalle_venta', 'idproducto', 'idventa', 'valor', 'descuento', 'iva', 'total'], 'integer'],
            [['descripcion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DetalleVenta::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iddetalle_venta' => $this->iddetalle_venta,
            'idproducto' => $this->idproducto,
            'idventa' => $this->idventa,
            'valor' => $this->valor,
            'descuento' => $this->descuento,
            'iva' => $this->iva,
            'total' => $this->total,
        ]);

        $query->andFilterWhere(['like', 'descripcion', $this->descripcion]);

        return $dataProvider;
    }
}
