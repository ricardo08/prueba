<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%producto}}".
 *
 * @property int $idproducto
 * @property string $descripcion
 * @property int $valor
 *
 * @property DetalleVenta[] $detalleVentas
 */
class Producto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%producto}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['descripcion', 'valor'], 'required'],
            [['valor'], 'integer'],
            [['descripcion'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idproducto' => Yii::t('app', 'Idproducto'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'valor' => Yii::t('app', 'Valor'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleVentas()
    {
        return $this->hasMany(DetalleVenta::className(), ['idproducto' => 'idproducto']);
    }
}
