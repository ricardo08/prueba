<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%venta}}".
 *
 * @property int $idventa
 * @property int $consecutivo
 * @property int $idcliente
 * @property string $fecha
 * @property string $hora
 * @property int $subtotal
 * @property int $descuento
 * @property int $iva
 * @property int $totalneto
 *
 * @property DetalleVenta[] $detalleVentas
 * @property Cliente $cliente
 */
class Venta extends \yii\db\ActiveRecord
{
    public $nombre;
    public $descripcion;
    public $valor;
    public $Total;
    public $Iva;
    //public $descuento;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%venta}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['consecutivo', 'idcliente', 'fecha', 'hora', 'subtotal', 'descuento', 'iva', 'totalneto'], 'required'],
            [['consecutivo', 'idcliente', 'subtotal', 'descuento', 'iva', 'totalneto'], 'integer'],
            [['fecha', 'hora'], 'safe'],
            [['idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['idcliente' => 'idcliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idventa' => Yii::t('app', 'Idventa'),
            'consecutivo' => Yii::t('app', 'Consecutivo'),
            'idcliente' => Yii::t('app', 'Idcliente'),
            'fecha' => Yii::t('app', 'Fecha'),
            'hora' => Yii::t('app', 'Hora'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'descuento' => Yii::t('app', 'Descuento'),
            'iva' => Yii::t('app', 'Iva'),
            'totalneto' => Yii::t('app', 'Totalneto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetalleVentas()
    {
        return $this->hasMany(DetalleVenta::className(), ['idventa' => 'idventa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'idcliente']);
    }
}
