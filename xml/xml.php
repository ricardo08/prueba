
<?php


 
function objectsIntoArray($arrObjData, $arrSkipIndices = array())
{
$arrData = array();
if (is_object($arrObjData)) {$arrObjData = get_object_vars($arrObjData);}
 
    if (is_array($arrObjData)) {
        foreach ($arrObjData as $index => $value) {
            if (is_object($value) || is_array($value)) {$value = objectsIntoArray($value, $arrSkipIndices);}
            if (in_array($index, $arrSkipIndices)) {continue;}
        $arrData[$index] = $value;
        }
    }
 
return $arrData;
}

 

//$arrXml = unserialize(stripcslashes($ArrayCacheDatos));



$xmlUrl = "http://xml.tutiempo.net/xml/70140.xml";
$xmlStr = file_get_contents($xmlUrl);
$xmlObj = simplexml_load_string($xmlStr);
$arrXml = objectsIntoArray($xmlObj);



 

$SalidaImprimir = '
<h1>El tiempo en '.$arrXml['localidad']['pais'].'</h1>
<h1>'.$arrXml['localidad']['nombre'].'</h1>
';
 
$dateH = date('Y-m-d'); 
foreach ($arrXml['pronostico_dias']['dia'] as $valor)
{
	if ($valor['fecha'] == $dateH) {
		
	
$SalidaImprimir .= '
<table cellpadding="0" cellspacing="10">
<tr>
<th align="left" colspan="2"><strong>'.$valor['fecha'].'</strong></th>
<th>Máxima</th>
<th>Mínima</th>
</tr>
<tr>
<td><img alt="'.$valor['texto'].'" height="50" src="'.$valor['icono'].'" width="50" /></td>
<td>'.$valor['texto'].'</td>
<td align="center" class="max">'.$valor['temp_maxima'].' &deg;C</td>
<td align="center" class="min">'.$valor['temp_minima'].' &deg;C</td>
</tr>
</table>';
}
}
 

echo $SalidaImprimir;
 
?>