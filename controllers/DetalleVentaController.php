<?php

namespace app\controllers;

use Yii;
use app\models\DetalleVenta;
use app\models\DetalleVentaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Venta;
use app\models\Producto;

/**
 * DetalleVentaController implements the CRUD actions for DetalleVenta model.
 */
class DetalleVentaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DetalleVenta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DetalleVentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DetalleVenta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DetalleVenta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id)
    {
        $model = new DetalleVenta();
        $venta = Venta::findOne(['idventa' => $id]);
        //var_dump($venta); exit();
        if ($model->load(Yii::$app->request->post())) {
            $model->total = 0;
            $model->valor = 0;
            $model->idventa = $id;
             $model->save();
             $producto = Producto::findOne(["idproducto" => $model->idproducto]);
             
             $model->valor = $producto->valor;
             $model->save();
             //var_dump($model->valor); exit();
             $des = intval($model->valor * ($model->descuento/100));
                 $totalD = $model->valor - $des;
                 
            $iva = intval($totalD * ($model->iva / 100));
           //var_dump($iva);
            $model->total = $totalD + $iva;
            //var_dump($model->total); exit();
            //$producto = Producto::findOne(["idproducto" => $model->idproducto]);
            //var_dump($producto->descripcion); exit();
            
            $model->save();

            return $this->redirect(['venta/index']);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing DetalleVenta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->iddetalle_venta]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DetalleVenta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DetalleVenta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DetalleVenta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DetalleVenta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
