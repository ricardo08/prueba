<?php

namespace app\controllers;

use Yii;
use app\models\Venta;
use app\models\DetalleVenta;
use app\models\VentaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;

/**
 * VentaController implements the CRUD actions for Venta model.
 */
class VentaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Venta models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VentaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Venta model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Venta model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Venta();

        if ($model->load(Yii::$app->request->post())) {
                
                $customers = Venta::find('consecutivo')
                ->select('consecutivo')
                ->orderBy('consecutivo DESC')
                ->limit(1)
                ->One();
                //var_dump($customers->consecutivo);exit();
            if (isset($customers->consecutivo)) {
                 
                 $model->consecutivo = $customers->consecutivo + 1;
                 $model->save();
            }else{
                $model->consecutivo = 100;
            }
            
            //var_dump($customers); exit();
           
            $model->fecha = date("Y-m-d (H:i:s)");
            $hora = time();
            $model->hora = date("H:i:s", $hora);
            $model->subtotal = 0;
            $model->totalneto = 0;
            $model->save();  

            return $this->redirect(['view', 'id' => $model->idventa]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    // funcion de factura

    public function actionFactura($id)
    {
            $totales = DetalleVenta::find()->where(['idventa'=> $id])->sum("total");
            
            var_dump($totales);// exit(); 
            $model = Venta::findOne(['idventa'=> $id]);
            if ($model->subtotal == 0) {
                 $model->subtotal = $totales;

             $des = intval($model->subtotal * ($model->descuento/100));
                 $totalD = $model->subtotal - $des;
                 
            $iva = intval($totalD * ($model->iva / 100));
           //var_dump($iva);
            $model->totalneto = $totalD + $iva;
            $model->save();
            
            //return;

             $table = new Venta();
                $model = $table->find()->all();

                $sqlF = "SELECT producto.descripcion as descripcion, detalle_venta.valor as valor, detalle_venta.descuento as descuento,
                            detalle_venta.iva as Iva,detalle_venta.total as Total, subtotal, totalneto AS todo FROM venta
                            INNER JOIN detalle_venta ON detalle_venta.idventa = venta.idventa
                            INNER JOIN producto ON producto.idproducto = detalle_venta.idproducto
                            WHERE venta.idventa =$id";

                        $factura = $table->findBySql($sqlF)->all();
                       // var_dump($factura); exit();

                $sqlSub = "SELECT DISTINCT 
                             subtotal, totalneto  FROM venta
                            WHERE venta.idventa =$id";

                        $sub = $table->findBySql($sqlSub)->all();
                       // var_dump($sub); exit();

                $sqlNFactura = "SELECT DISTINCT consecutivo,  fecha, iva, descuento  FROM venta
                            WHERE venta.idventa =$id";

                        $cons = $table->findBySql($sqlNFactura)->all();
                       // var_dump($factura); exit();
                $sqlCliente = "SELECT DISTINCT nombre FROM cliente INNER JOIN venta ON venta.idcliente = cliente.idcliente 
                                WHERE venta.idventa =  $id";

                        $cliente = $table->findBySql($sqlCliente)->all();

                         $mPDF1 = new  \Mpdf\Mpdf([
                        'tempDir' => __DIR__ . '/tmp',
                        'mode' => 'utf-8',
                        'format' => 'A4',
                        'orientation' => 'L'
                ]); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
                //$mPDF1->useOnlyCoreFonts = true;
                //$mPDF1->SetTitle("Ordenes - Reporte");
                //$mPDF1->SetAuthor("Computel");
                $mPDF1->SetWatermarkText("");
                $mPDF1->showWatermarkText = true;
                $mPDF1->watermark_font = 'DejaVuSansCondensed';
                $mPDF1->watermarkTextAlpha = 0.1;
                $mPDF1->SetDisplayMode('fullpage');
                $mPDF1->WriteHTML($this->renderPartial('factura', ['factura'=>$factura, 'cliente' => $cliente, 'cons' => $cons, 'sub' => $sub], true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
                $mPDF1->Output('Factura'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
                exit;


            }else{
                $model->subtotal = $totales;

             $des = intval($model->subtotal * ($model->descuento/100));
                 $totalD = $model->subtotal - $des;
                 
            $iva = intval($totalD * ($model->iva / 100));
           //var_dump($iva);
            $model->totalneto = $totalD + $iva;
            $model->save();

                $table = new Venta();
                $model = $table->find()->all();

                $sqlF = "SELECT producto.descripcion as descripcion, detalle_venta.valor as valor, detalle_venta.descuento as descuento,
                            detalle_venta.iva as Iva,detalle_venta.total as Total, subtotal, totalneto AS todo FROM venta
                            INNER JOIN detalle_venta ON detalle_venta.idventa = venta.idventa
                            INNER JOIN producto ON producto.idproducto = detalle_venta.idproducto
                            WHERE venta.idventa =$id";

                        $factura = $table->findBySql($sqlF)->all();
                       // var_dump($factura); exit();

                $sqlSub = "SELECT DISTINCT 
                             subtotal, totalneto  FROM venta
                            WHERE venta.idventa =$id";

                        $sub = $table->findBySql($sqlSub)->all();
                       // var_dump($sub); exit();

                $sqlNFactura = "SELECT DISTINCT consecutivo,  fecha, iva, descuento  FROM venta
                            WHERE venta.idventa =$id";

                        $cons = $table->findBySql($sqlNFactura)->all();
                       // var_dump($factura); exit();
                $sqlCliente = "SELECT DISTINCT nombre FROM cliente INNER JOIN venta ON venta.idcliente = cliente.idcliente 
                                WHERE venta.idventa =  $id";

                        $cliente = $table->findBySql($sqlCliente)->all();

                         $mPDF1 = new  \Mpdf\Mpdf([
                        'tempDir' => __DIR__ . '/tmp',
                        'mode' => 'utf-8',
                        'format' => 'A4',
                        'orientation' => 'L'
                ]); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
                //$mPDF1->useOnlyCoreFonts = true;
                //$mPDF1->SetTitle("Ordenes - Reporte");
                //$mPDF1->SetAuthor("Computel");
                $mPDF1->SetWatermarkText("");
                $mPDF1->showWatermarkText = true;
                $mPDF1->watermark_font = 'DejaVuSansCondensed';
                $mPDF1->watermarkTextAlpha = 0.1;
                $mPDF1->SetDisplayMode('fullpage');
                $mPDF1->WriteHTML($this->renderPartial('factura', ['factura'=>$factura, 'cliente' => $cliente, 'cons' => $cons, 'sub' => $sub], true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
                $mPDF1->Output('Factura'.date('YmdHis'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
                exit;

                        
                //echo "ya esta hecho";
            }
           

        
               
    }

    /**
     * Updates an existing Venta model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idventa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Venta model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Venta model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Venta the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Venta::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
