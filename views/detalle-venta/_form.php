<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Venta;
use app\models\Producto;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleVenta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="detalle-venta-form">

    <?php $form = ActiveForm::begin(); ?>

     <?= $form->field($model, 'idproducto')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Producto::find()->all(), 'idproducto', 'descripcion'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione el producto'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>

    
   <!--  <?= $form->field($model, 'idventa')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Venta::find()->all(), 'idventa', 'consecutivo'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione el cliente'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?> -->

   <?= $form->field($model, 'descripcion')->textInput(['maxlength' => true]) ?> 

    <!--  <?= $form->field($model, 'valor')->textInput() ?> -->

    <?= $form->field($model, 'descuento')->textInput() ?>

    <?= $form->field($model, 'iva')->textInput() ?>

<!--     <?= $form->field($model, 'total')->textInput() ?>  -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
