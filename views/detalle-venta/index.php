<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\DetalleVentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Detalle Ventas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="detalle-venta-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Detalle Venta'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'iddetalle_venta',
            //'idproducto',
             [ // asi se establece un campo de otra tabla con el searching GridView
              'label' => 'Producto',
              'attribute' => 'idproducto',
              'value' =>    'producto.descripcion',
               'contentOptions' => ['style' => 'width:auto; white-space: normal;'],
            ],
            //'idventa',
             [ // asi se establece un campo de otra tabla con el searching GridView
              'label' => 'Cliente',
              'attribute' => 'idventa',
              'value' =>    'venta.cliente.nombre',
               'contentOptions' => ['style' => 'width:auto; white-space: normal;'],
            ],
            'descripcion',
            'valor',
            //'descuento',
            //'iva',
            //'total',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
