<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DetalleVenta */

$this->title = Yii::t('app', 'Update Detalle Venta: {name}', [
    'name' => $model->iddetalle_venta,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Detalle Ventas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->iddetalle_venta, 'url' => ['view', 'id' => $model->iddetalle_venta]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="detalle-venta-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
