<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Venta */

$this->title = $model->idventa;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ventas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="venta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <!-- <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idventa], ['class' => 'btn btn-primary']) ?> -->
         <?= Html::a(Yii::t('app', 'Crear Detalle'), ['detalle-venta/create', 'id' => $model->idventa], ['class' => 'btn btn-primary']) ?>
        <!-- <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idventa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?> -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idventa',
            'consecutivo',
            'idcliente',
            'fecha',
            'hora',
            'subtotal',
            'descuento',
            'iva',
            'totalneto',
        ],
    ]) ?>

</div>
