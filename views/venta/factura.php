<html>
<head>
<style>
body,html{
  font-family: Arial;
height:100%; /*Siempre es necesario cuando trabajamos con alturas*/
}
 #inferior{
position:absolute; /*El div será ubicado con relación a la pantalla*/
left:0px; /*A la derecha deje un espacio de 0px*/
right:0px; /*A la izquierda deje un espacio de 0px*/
bottom:0px; /*Abajo deje un espacio de 0px*/
height:270px; /*alto del div*/

 }

 p { margin: 0pt;
 }
 th {
    background-color: #1f1fdf;
    color: white;
}
</style>
</head>
<body>
<!--mpdf
<htmlpageheader name="myheader">
<div align = "center">
<strong>LA 13</strong>
</div><br />

</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; text-align: center; padding-top: 3mm; ">
<span font-size: 10pt;><strong>SUPERMERCADO LA 13 </strong>
<br />
CARRERA 6 # 15N 09 TELFAX 8201210 CEL 324 6506611  POPAYÁN – CAUCA <br />
E-mail: la13.popayan@gmail.com 
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
<sethtmlpagefooter name="myfooter" value="on" />
mpdf-->
 <!-- <br />
 <br /><br /><br /><br /><br /><br />
<div style="text-align: right"><b>Fecha: </b><?php echo date("d/m/Y"); ?> </div>
<b> -->
<br />
<br /><br /><br /><br /><br /><br />
<div>
      <div style="float:left;width: 70%;outline: green solid thin">
        <table>
          <tr>
            <td>
            <h3>FACTURA DE VENTA <font color="red"> <?php foreach($cons as $row): echo $row->consecutivo; endforeach ?> </font> </h3>
            </td>
          </tr>
          <tr>
            <td>
            Señor (es)<br />
            <strong><?php foreach($cliente as $row): echo $row->nombre; endforeach ?> </strong>
            </td>
          </tr>
        </table>
      </div>
      <div style="float:left;width: 30%;outline: red solid thin">
        <table>
          <tr>
            <td>Fecha:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php foreach($cons as $row): echo $row->fecha; endforeach ?></td>
          </tr>
          <tr>
        </table>

      </div>
</div>
<br />
 <table width="100%"  >
 <thead>
 <tr>

 <th >DESCRIPCIÓN</th>
 <th >PRECIO</th>
 <th >DESCUENTO</th>
  <th >IVA</th>
 <th >TOTAL</th>
 </tr>
 </thead>
 <tbody>

 <?php foreach($factura as $row): ?>

 <tr>
 
 <td align="center">
 <?php echo $row->descripcion; ?>
 </td>
 <td align="center">
 <?php echo $row->valor; ?>
 </td>
  <td align="center">
 <?php echo "$row->descuento %"; ?>
 </td>
  <td align="center">
 <?php echo "$row->Iva %"; ?>
 </td>
 <td align="center">
 <?php echo $row->Total; ?>
 </td>

 </tr>
 <?php endforeach; ?>
 <!-- FIN ITEMS -->
 <tr>
 <td class="blanktotal" colspan="5" rowspan="1"></td>
 </tr>
 </tbody>
 </table>
 <div id="inferior">
    <div style="float:center;width: 100%;outline: green solid thin">
      <table  align= "center">
        <tr>
          <td>
          <h4>SUB TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h4>
          </td>
            <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <?php foreach($sub as $row): echo intval($row->subtotal); endforeach ?> </td>
        </tr>
         <tr>
          <td>
          <h4>IVA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h4>
          </td>
            <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <?php foreach($cons as $row): echo "$row->iva %"; endforeach ?> </td>
        </tr>
        <tr>
          <td>
          <h4>DESCUENTO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </h4>
          </td>
            <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <?php foreach($cons as $row): echo "$row->descuento %"; endforeach ?> </td>
        </tr>
        <tr>
          <td>
          <h4>TOTAL A PAGAR</h4><br />
          </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <?php foreach($sub as $row): echo $row->totalneto ; endforeach ?> </td>
        </tr>
      </table>
    </div>

</div>
 </body>

 </html>
