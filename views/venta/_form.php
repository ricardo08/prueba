<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\models\cliente;

/* @var $this yii\web\View */
/* @var $model app\models\Venta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="venta-form">

    <?php $form = ActiveForm::begin(); ?>

     <!-- <?= $form->field($model, 'consecutivo')->textInput() ?>  -->

    <?= $form->field($model, 'idcliente')->widget(Select2::classname(), [
            'data' => ArrayHelper::map(Cliente::find()->all(), 'idcliente', 'nombre'),
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccione el cliente'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);?>

    <!-- <?= $form->field($model, 'subtotal')->textInput() ?> -->

    <?= $form->field($model, 'descuento')->textInput() ?>

    <?= $form->field($model, 'iva')->textInput() ?>

<!-- <?= $form->field($model, 'totalneto')->textInput() ?>  -->

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
