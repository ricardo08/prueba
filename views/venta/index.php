<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\VentaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ventas');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="venta-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Venta'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'idventa',
            [   'label' => "# Factura",
                'attribute' =>'consecutivo'],
                [ // asi se establece un campo de otra tabla con el searching GridView
              'label' => 'Nombre del Cliente',
              'attribute' => 'idcliente',
              'value' =>    'cliente.nombre',
               'contentOptions' => ['style' => 'width:auto; white-space: normal;'],
            ],
            
            
            //'idcliente',
            'fecha',
            'hora',
            //'subtotal',
            'descuento',
            'iva',
            'totalneto',

            [ 'class' => 'yii\grid\ActionColumn',
             'template' => '{create}{factura}{delete}',
            'buttons' => [
              'create' => function ($url, $model) {
           return Html::a(
               '<span class="glyphicon glyphicon-plus"></span>',
               ['detalle-venta/create', 'id' => $model->idventa],
               [
                   'title' => 'detalle',
                   'data-pjax' => '0',
               ]
           );
       },
          'factura' => function ($url, $model) {
           return Html::a(
               '<span class="glyphicon glyphicon-file"></span>',
               ['venta/factura', 'id' => $model->idventa],
               [
                   'title' => 'factura',
                   'data-pjax' => '0',
               ]
           );
       },


               ],
           ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
